Select *
  INTO #EcTemp
  FROM [AppCity].[dbo].[DC_DataCollection]
  where SourceDesc = 'WWT'; -- <UPDATE>

  INSERT INTO [EC].[dbo].[EcPartData] (
	   [BomType]
      ,[Customer]
      --,[Content_ID]
      ,[IPN]
      ,[Supplier]
      ,[CatalogPN]
      ,[CatalogPNWP]
      --,[Revision]
      ,[Description]
      ,[TppManufacturer_ID]
      ,[TppCatalogPN]
      ,[TppCatalogPNWP]
      --,[ReworkNote_ID]
      ,[CommentToCustomer]
      ,[RohsAlternate]
      ,[SentEmail]
      ,[ComplianceURL]
      ,[EcComment]
      ,[Status_ID]
      ,[Technician_ID]
      ,[RecordDueDate]
      ,[RecordPriority]
      --,[MfgResponseLog_ID]
      ,[McActionTaken_ID]
      ,[MCActionTakenDate]
      ,[NCA_ID]
      ,[ProjectLeadComment]
      ,[McApproval]
      ,[McApprovedDate]
      ,[NcaApproval]
      ,[NcaApprovedDate]
      --,[ArchiveApproval]
      --,[ArchiveApprovedDate]
      --,[InMcOverflow]
      ,[RecontactMcCount]
      --,[Archived]
      --,[ArchivedBy]
      --,[ArchivedDate]
      --,[ArchivedNote_ID]
      --,[CheckedOutBy]
      ,[TechnicianWorkedDate]
      --,[Import_ID]
      ,[CreatedBy]
      ,[CreateDate]
      ,[LWhoDid]
      ,[LastUpdate]
      ,[MasterList_ID]
      ,[Account_ID]
	)
  Select
	   [Source]--[BomType]
      ,'World Wide Technology' -- <UPDATE>
      --,[Content_ID]
      ,[C_RefPN] --[IPN]
      ,[C_MFG]--[Supplier]
      ,[C_MfgPN]--[CatalogPN]
      ,[C_PNWP]--[CatalogPNWP]
      --,[Revision]
      ,[C_Description] --[Description]
      ,[TPP_Mfg_ID]--[TppManufacturer_ID]
      ,[TPP_MfgPN]--[TppCatalogPN]
      ,[TPP_PNWP]--[TppCatalogPNWP]
      --,[ReworkNote_ID]
      ,''--[CommentToCustomer]
      ,[Alternate]--[RohsAlternate]
      ,[ContentDeclarationURL]--[SentEmail]
      ,[RoadmapURL]--[ComplianceURL]
      ,[DC_Comment]--[EcComment]
      ,[DataCollectionStatus_ID]--[Status_ID]
      ,[Technician_ID]--[Technician_ID]
      ,[RecordDueDate]--[RecordDueDate]
      ,[RecordPriority]--[RecordPriority]
      --,[MfgResponseLog_ID]--[MfgResponseLog_ID]
      ,[McActionTaken_ID]--[McActionTaken_ID]
      ,[MCActionTakenDate]--[MCActionTakenDate]
      ,[NCA_ID]--[NCA_ID]
      ,[ProjectLeadComment]--[ProjectLeadComment]
      ,[McApproval]--[McApproval]
      ,[McApprovedDate] --[McApprovedDate]
      ,[NcaApproval]--[NcaApproval]
      ,[NcaApprovedDate]--[NcaApprovedDate]
      --,[ArchiveApproval]
      --,[ArchiveApprovedDate]
      --,[InMcOverflow]
      ,[RecontactMcCount]--[RecontactMcCount]
      --,[Archived]
      --,[ArchivedBy]
      --,[ArchivedDate]
      --,[ArchivedNote_ID]
      --,[CheckedOutBy]--[CheckedOutBy]
      ,[TechnicianWorkedDate]--[TechnicianWorkedDate]
      --,[Import_ID]
      ,[CreatedBy]--[CreatedBy]
      ,[CreateDate]--[CreateDate]
      ,[LWhoDid]--[LWhoDid]
      ,[LastUpdate]--[LastUpdate]
      ,110337 -- Master List ID <UPDATE>
      ,'0A467519-420E-45C0-9C52-6A3316E3CC68' --Account ID <UPDATE>
  FROM #EcTemp
  where SourceDesc = 'WWT'; -- <UPDATE>

EXEC [EC].[dbo].[CreateMissingLogEntries] 389;

EXEC [EC].[dbo].[AssignContentIdToAllRecords];

update [EC].[dbo].[MfgResponseLog]
set Part_ID = epd.Part_ID,
	CreatedBy = l.CreatedBy,
	CreateDate = l.CreateDate,
	LastUpdate = l.LastUpdate,
	LWhoDid = l.LWhoDid,
	ActionTaken_ID = l.ActionTaken_ID,
	ActionResponse_ID = l.ActionResponse_ID,
	ActionComment = l.ActionResponseNote,
	ContactName = l.ContactName,
	ContactEmail = l.ContactEmail,
	ContactPhone = l.ContactPhone
from #EcTemp e
join [AppCity].[dbo].[DC_MfgResponseLog] l on e.DataCollection_ID = l.DataCollection_ID
join [EC].[dbo].[EcPartData] epd on
	e.C_RefPN = epd.IPN and
	e.SourceDesc = epd.Customer and
	e.C_MFG = epd.Supplier and
	e.[C_MfgPN] = epd.CatalogPN
where [EC].[dbo].[MfgResponseLog].MfgResponseLog_ID = epd.MfgResponseLog_ID;

DROP TABLE #EcTemp;
