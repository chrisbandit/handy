USE MC_Staging;
GO

-- Shows the last SVHC Level record entry
SELECT TOP 1 *
FROM dbo.MC_SVHC
WHERE [ListIdentity] LIKE 'EUREACH-%'
ORDER BY [DisplayOrder] DESC


-- Modify this query to have the correct values for the new SVHC level.
SET IDENTITY_INSERT dbo.MC_SVHC ON;
INSERT INTO dbo.MC_SVHC (
    [SVHC_ID], [SVHC], [SVHC_Code], [Description],
    [DisplayOrder], [ListIdentity], [CreatedBy], [LWhoDid])
SELECT
      [SVHC_ID]      = 21             -- One higher then the last SVHC_ID
    , [SVHC]         = 'SVHC 181'     -- 
    , [SVHC_Code]    = '181'          -- This is the SVHC number only
    , [Description]  = 'SVHC 181'     -- This always matches [SVHC]
    , [DisplayOrder] = 19             -- One higher then the last DisplayOrder
    , [ListIdentity] = 'EUREACH-0118' -- format: EUREACH-mmyy
    , [CreatedBy]    = 4              -- Your AppCity User_ID
    , [LWhoDid]      = 4;             -- Your AppCity User_ID
SET IDENTITY_INSERT dbo.MC_SVHC OFF;
