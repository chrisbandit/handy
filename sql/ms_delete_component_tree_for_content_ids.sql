USE MC_Staging;
GO

SET NOCOUNT ON;

IF (OBJECT_ID('tempdb..#CIDs') IS NOT NULL) DROP TABLE #CIDs
SELECT DISTINCT [Content_ID]
INTO #CIDs
FROM dbo.MC_Contents
WHERE [Content_ID] IN (
	71087, 77889, 77890, 77892, 77893, 77894, 77895, 77896, 77901, 139425, 139429, 139430, 139434, 184944, 191213, 191794, 191804, 191805,
	191807, 191815, 191822, 191826, 199695, 199725, 199728, 199739, 199741, 199742, 199745, 199753, 199843, 199851, 199855, 199859, 199860,
	199864, 206348, 206351, 208336, 208347, 208351, 208353, 208356, 208357, 208359, 208361, 208362, 208365, 208366, 208369, 208372, 208373,
	208374, 208377, 208379, 208384, 228649, 228654, 230044, 236744, 236745, 248172, 248190, 248192, 248194, 248204, 248225, 273955, 277784,
	277787, 282133
	);

DECLARE @ContentId INT

DECLARE @ErrorMessage  NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState    INT;

BEGIN TRANSACTION;
BEGIN TRY
	WHILE ((SELECT COUNT(*) FROM #CIDs) > 0)
	BEGIN
		SET @ContentId = (SELECT TOP 1 [Content_ID] FROM #CIDs)
		DELETE FROM #CIDs WHERE [Content_ID] = @ContentId

		PRINT 'Deleting the Component tree for CID: ' + ISNULL(CAST(@ContentId AS VARCHAR(50)), 'NULL')

		DELETE FROM dbo.MC_MaterialSubstances
		WHERE EXISTS (
				SELECT * FROM dbo.MC_ComponentMaterials CM
				WHERE EXISTS (
						SELECT * FROM dbo.MC_ContentComponents CC
						WHERE CC.[Content_ID] = @ContentId
						AND CC.[ContentComponent_ID] = CM.[ContentComponent_ID]
						)
				AND CM.[ComponentMaterial_ID] = dbo.MC_MaterialSubstances.[ComponentMaterial_ID]
				)

		DELETE FROM dbo.MC_ComponentMaterials
		WHERE EXISTS (
				SELECT * FROM dbo.MC_ContentComponents CC
				WHERE CC.[Content_ID] = @ContentId
				AND CC.[ContentComponent_ID] = dbo.MC_ComponentMaterials.[ContentComponent_ID]
				)

		DELETE FROM dbo.MC_Map_Assembly2Components
		WHERE EXISTS (
				SELECT * FROM dbo.MC_ContentComponents CC
				WHERE CC.[Content_ID] = @ContentId
				AND CC.[ContentComponent_ID] = dbo.MC_Map_Assembly2Components.[ContentComponent_ID]
				)
		OR EXISTS (
				SELECT * FROM dbo.MC_ContentAssemblies CA
				WHERE CA.[Content_ID] = @ContentId
				AND CA.[ContentAssembly_ID] = dbo.MC_Map_Assembly2Components.[ContentAssembly_ID]
				)

		DELETE FROM dbo.MC_ContentComponents
		WHERE [Content_ID] = @ContentId
			
		DELETE FROM dbo.MC_ContentAssemblies
		WHERE [Content_ID] = @ContentId

		UPDATE dbo.MC_Contents SET
			  [DeclarationType_ID] = 1
			, [LWhoDid] = 4
			, [LastUpdate] = GETDATE()
		WHERE [Content_ID] = @ContentId

	END

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;

	SELECT @ErrorMessage = ERROR_MESSAGE()
			, @ErrorSeverity = ERROR_SEVERITY()
			, @ErrorState = ERROR_STATE();
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH

IF (OBJECT_ID('tempdb..#CIDs') IS NOT NULL) DROP TABLE #CIDs