/*
 * This is meant to be run on the TPPSQL3 server.
 * Reference ticket : LEG-938
 * https://totalpartsplus.atlassian.net/browse/LEG-938
 */

DECLARE @SystemOrigin VARCHAR(50) = 'I'

-- Restore parts from previous AML
--------------------------------------------------------------------------------
IF (OBJECT_ID('tempdb..#fNOK') IS NOT NULL) DROP TABLE #fNOK
SELECT
	[IPN],[PartDescription],[MPN],[OPC],[SupplierName],[SupplierCode],[SystemOrigin],[CommodityCode],[CategoryCode],[Category],[AmlPartStatus],[C_PNWP],[IPN_PNWP],[SourceFile]
INTO #fNOK
FROM dbo.AlcatelAML_All_Last
WHERE [SystemOrigin] = @SystemOrigin


DELETE FROM dbo.AlcatelAML_All_Last
INSERT INTO dbo.AlcatelAML_All_Last (
	[IPN],[PartDescription],[MPN],[OPC],[SupplierName],[SupplierCode],[SystemOrigin],[CommodityCode],[CategoryCode],[Category],[AmlPartStatus],[C_PNWP],[IPN_PNWP],[SourceFile])
SELECT
	[IPN],[PartDescription],[MPN],[OPC],[SupplierName],[SupplierCode],[SystemOrigin],[CommodityCode],[CategoryCode],[Category],[AmlPartStatus],[C_PNWP],[IPN_PNWP],[SourceFile]
FROM dbo.AlcatelAML_All


DELETE FROM dbo.AlcatelAML_All WHERE [SystemOrigin] = @SystemOrigin
INSERT INTO dbo.AlcatelAML_All (
	[IPN],[PartDescription],[MPN],[OPC],[SupplierName],[SupplierCode],[SystemOrigin],[CommodityCode],[CategoryCode],[Category],[AmlPartStatus],[C_PNWP],[IPN_PNWP],[SourceFile])
SELECT
	[IPN],[PartDescription],[MPN],[OPC],[SupplierName],[SupplierCode],[SystemOrigin],[CommodityCode],[CategoryCode],[Category],[AmlPartStatus],[C_PNWP],[IPN_PNWP],[SourceFile]
FROM #fNOK



-- Update DIFF log and search
--------------------------------------------------------------------------------
EXEC dbo.AlcatelAML_AddAMLDiffsToLog
EXEC dbo.AlcatelAML_GenerateSearchTables


-- Send Import report
--------------------------------------------------------------------------------
-- EXEC dbo.AlcatelAML_ValidateImportAndReport