DEV

select COUNT(*) from BOMsPartList bpl
where bpl.BomId = 138706

select COUNT(*) from BOMsPartList bpl
where bpl.BomId = 137692


select COUNT(*) from BOMsPartList bpl
where bpl.BomId = 138709


select COUNT(*) from BOMsPartList bpl
where bpl.BomId = 127778

select distinct CBD.ResearchStatus from WeeklyUpdate..CmmBomData CBD
where CBD.ResearchStatus = 'In Research'

INSERT INTO dbo.Parts_ResearchStatus (
	[AccountId], [PartId], [ResearchStatusId])
SELECT
	R.[AccountId], R.[PartId], RS.[id]
FROM dbo.PartsData_InResearch R
JOIN dbo.ResearchStatus RS ON RS.[ResearchStatus] = R.[ResearchStatus]
WHERE NOT EXISTS (
		SELECT * FROM dbo.Parts_ResearchStatus T
		WHERE T.[AccountId] = R.[AccountId]
		AND T.[PartId] = R.[PartId])