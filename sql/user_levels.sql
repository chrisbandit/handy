-- get users for an application
SELECT *
FROM [AppCity].[dbo].[Admin_Users]
WHERE LOWER(UserName) like '%nathen%'
  
-- Find what an application's id is
SELECT * FROM [AppCity].[dbo].[Admin_Applications]
  
-- find out your settings...
SELECT * from dbo.Admin_UserSettings aus
WHERE aus.User_ID = 379

-- Get your security levels for an application
SELECT * FROM Admin_UserApplicationSecurity
WHERE Application_ID = 27
AND User_ID = 379

-- Figure out what those security levels mean...
SELECT uas.*, usl.SecurityName, usl.SecurityValue FROM Admin_UserApplicationSecurity uas
LEFT JOIN Admin_SecurityLevels usl ON (usl.SecurityLevel_ID = uas.SecurityLevel_ID)
WHERE uas.Application_ID = 27


-- figure out which one is administrator (it's 1)
SELECT * FROM Admin_SecurityLevels

-- promote yourself :D
UPDATE Admin_UserApplicationSecurity
SET SecurityLevel_ID = 2
WHERE Application_ID = 27
AND USER_ID = 379

-- USER id = 379, EC tool = 27