 INSERT INTO [PartsData_new] 
				(RefPN, MFGPN, PNWP, Manufacturer, Manufacturer_ID, OriginalPN, OriginalMfg, OriginalMfg_ID, OriginalDescription, OriginalRevision, Content_ID, HasConflictMetals, 
				HasConflictDoc, HasEICCGeSIDoc, HalogenFree, Proprietary, SVHCFree, RoHS, RoHS_NonCompliant, RoHS4Present, UnitType, DeclarationType_ID, Disclosure, HasExemptions, 
				LeadFree, SVHC_SunsetDate, ResearchStatus, NCA_ID, CMReportVersion) 
			SELECT RefPN, MFGPN, PNWP, Manufacturer, Manufacturer_ID, OriginalPN, OriginalMfg, OriginalMFG_ID, OriginalDescription, OriginalRevision, Content_ID, HasConflictMetals, 
				HasConflictDoc, HasEICCGeSIDoc, HalogenFree_Derived, Proprietary_Derived, SVHCFree_Derived, RoHS_Derived, RoHS_NonCompliant_Derived, RoHS4Present_Derived, UnitType, 
				DeclarationType_ID, DeclarationType, HasExemptions, LeadFree, 
				SVHC_SunsetDate_Derived, ResearchStatus, NCA_ID, CMReportVersion 
		    FROM ( 
					SELECT DISTINCT 
						ROW_NUMBER() OVER (PARTITION BY B.RefPN, B.MfgPN + B.Manufacturer + OriginalPN + OriginalMfg + OriginalDescription + OriginalRevision 
						ORDER BY C.IsPreferredContent DESC) AS ROW_ID, 
						B.RefPN + B.MfgPN + CAST(B.Manufacturer_ID AS VARCHAR(10)) AS PN_ID, 
						AppCity..MC_PNs.MCPN_ID, 
						B.RefPN, 
						B.MfgPN, 
						B.PNWP, 
						B.Manufacturer, 
						B.Manufacturer_ID, 
						B.OriginalPN, 
						B.OriginalMfg, 
						B.OriginalMfg_ID, 
						B.OriginalDescription OriginalDescription, 
						B.OriginalRevision OriginalRevision, 
						CASE WHEN AppCity..MC_PNs.Content_ID IS NULL THEN 0 ELSE AppCity..MC_PNs.Content_ID END AS Content_ID, 
						CASE WHEN HasConflictMetals IS NULL THEN '' ELSE HasConflictMetals END AS HasConflictMetals, 
						CASE WHEN HasConflictDoc IS NULL THEN '' ELSE HasConflictDoc END AS HasConflictDoc, 
						CASE WHEN HasEICCGeSIDoc IS NULL THEN '' ELSE HasEICCGeSIDoc END AS HasEICCGeSIDoc, 
						CASE WHEN C.HalogenFree_Derived IS NULL THEN '' ELSE C.HalogenFree_Derived END HalogenFree_Derived, 
						CASE WHEN C.Proprietary_Derived IS NULL THEN '' ELSE C.Proprietary_Derived END Proprietary_Derived, 
						CASE WHEN C.SVHCFree_Derived IS NULL THEN '' ELSE C.SVHCFree_Derived END SVHCFree_Derived, 
						CASE WHEN C.RoHS_Derived IS NULL THEN '' ELSE C.RoHS_Derived END AS RoHS_Derived, 
						CASE WHEN C.RoHS_NonCompliant_Derived IS NULL THEN '' ELSE C.RoHS_NonCompliant_Derived END AS RoHS_NonCompliant_Derived, 
						CASE WHEN C.RoHS4Present_Derived IS NULL THEN '' ELSE C.RoHS4Present_Derived END AS RoHS4Present_Derived, 
						CASE WHEN AppCity..MC_UnitTypes.IpcLabel IS NULL THEN 'Each' ELSE AppCity..MC_UnitTypes.IpcLabel END AS UnitType, 
						C.DeclarationType_ID, 
						CASE WHEN DeclarationType IS NULL THEN 'Unknown' ELSE DeclarationType END AS DeclarationType, 
						CASE WHEN HasExemptions IS NULL THEN '' ELSE HasExemptions END HasExemptions, 
						CASE WHEN IsLeadFree = 1 THEN 'Yes' ELSE '' END LeadFree, 
						CASE WHEN C.SVHC_SunsetDate_Derived IS NULL THEN '' ELSE C.SVHC_SunsetDate_Derived END AS SVHC_SunsetDate_Derived, 
						IsNull(ResearchStatus, '') ResearchStatus, NCA_ID, IsNull(CMReportVersion, '') CMReportVersion 
					FROM (SELECT RefPN, MfgPN, PNWP, Manufacturer, Manufacturer_ID, OriginalPN, OriginalMfg, OriginalMFG_ID, OriginalDescription, OriginalRevision, 
					      ResearchStatus, NCA_ID FROM T_cognex) B 
					LEFT JOIN AppCity..MC_PNs 
						ON B.PNWP = AppCity..MC_PNs.PNWP 
						AND AppCity..MC_PNs.Manufacturer_ID = B.Manufacturer_ID 
				    LEFT JOIN AppCity..MC_Contents C 
						ON AppCity..MC_PNs.Content_ID = C.Content_ID 
					LEFT JOIN AppCity..MC_DeclarationTypes T8 
						ON T8.DeclarationType_ID = C.DeclarationType_ID 
					LEFT JOIN AppCity..MC_UnitTypes 
						ON C.UnitType_ID = AppCity..MC_UnitTypes.UnitType_ID 
					LEFT JOIN ( 
							SELECT CD.Content_ID, D.AppURL 
							FROM AppCity..MC_ContentDocuments CD 
							INNER JOIN AppCity..MC_Documents D 
								ON CD.Document_ID = D.Document_ID
							WHERE DocType_ID = 12 
							AND InternalUseOnly = 0 
						) T9 
						ON C.Content_ID = T9.Content_ID 
				) Z
			WHERE [ROW_ID] = 1
			ORDER BY [PN_ID] DESC