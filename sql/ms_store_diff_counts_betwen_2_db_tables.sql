UPDATE ta set extra_part_count = (
    SELECT COUNT(*) from ((
      SELECT PDC.AccountId
      FROM CMM_ControlCopy..PartsData PDC 
      LEFT JOIN CMM..PartsData PDN ON (
          PDC.AccountId = PDN.AccountId
          AND PDC.RefPN = PDN.RefPN
          AND PDC.CatalogPNWP = PDN.CatalogPNWP
          AND PDC.Manufacturer_ID = PDN.Manufacturer_ID
          AND PDC.OriginalPN = PDN.OriginalPN
      )
      WHERE PDC.AccountId = ta.AccountID
          AND PDN.AccountId IS NULL
      )
 ) as t1
 ),
part_data_count = (
     SELECT COUNT(1) from CMM_ControlCopy..PartsData PD where ta.AccountID = PD.AccountId
)
FROM temp_AllAccounts_chris ta

CREATE TABLE temp_AllAccounts_chris (
    AccountID uniqueidentifier,
    AccountName varchar(MAX),
    part_data_count int default 0,
    extra_part_count int default 0,
)

DROP TABLE temp_AllAccounts_chris

INSERT INTO temp_AllAccounts_chris (AccountID, AccountName)
select distinct C.AccountID, C.Account from WeeklyUpdate..CmmBomData C

select * from temp_AllAccounts_chris