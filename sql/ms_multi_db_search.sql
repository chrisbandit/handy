EXECUTE master.sys.sp_MSforeachdb N'
	USE [?]; 
	IF (''?'' NOT LIKE ''%master%'' AND ''?'' NOT LIKE ''%model%'')
	BEGIN
		SELECT DB_NAME() as ''Database'';
		SELECT so.name as ''Table name'', 
			   sc.name as ''Column name'' 
	    FROM sys.objects as so
		INNER JOIN sys.columns as sc
			ON (sc.object_id = so.object_id)
		WHERE so.type_desc = ''USER_TABLE''
		AND LOWER(sc.name) like ''%heyman%''
		ORDER BY so.name, sc.name
	END'
GO