DECLARE @ContentID INT = 319090

SET NOCOUNT ON;

DECLARE @TranCounter INT;
SET @TranCounter = @@TRANCOUNT;
IF (@TranCounter > 0)
	SAVE TRANSACTION ProcedureSave;
ELSE
	BEGIN TRANSACTION;

BEGIN TRY

	SET IDENTITY_INSERT dbo.MC_Substances ON
	INSERT INTO dbo.MC_Substances (
		[Substance_ID],[Substance],[Symbol],[CAS],[EC],[JGPSSI],[SubstanceType_ID],[SubstanceCode_ID],[JigSubstanceCode_ID],[IecSubstanceType_ID],[IecSubstanceCode_ID],
		[SVHC_Level],[ReachSVHC],[Reach_ID],[SVHC_Auth],[SVHC_SunsetDate],[KREACH],[RoHS_Substance],[Halogen],[ConflictMetal],[Proprietary],[BoseProprietary],[BoseVerified],
		[Comments],[Prop65],[Japan_Substance],[ABB],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Toxicity],[NSRL_MADL])
	SELECT
		[Substance_ID],[Substance],[Symbol],[CAS],[EC],[JGPSSI],[SubstanceType_ID],[SubstanceCode_ID],[JigSubstanceCode_ID],[IecSubstanceType_ID],[IecSubstanceCode_ID],
		[SVHC_Level],[ReachSVHC],[Reach_ID],[SVHC_Auth],[SVHC_SunsetDate],[KREACH],[RoHS_Substance],[Halogen],[ConflictMetal],[Proprietary],[BoseProprietary],[BoseVerified],
		[Comments],[Prop65],[Japan_Substance],[ABB],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Toxicity],[NSRL_MADL]
	FROM SQL3.AppCity.dbo.MC_Substances I
	WHERE I.[Substance_ID] NOT IN (SELECT [Substance_ID] FROM dbo.MC_Substances)
	PRINT 'INSERT MC_Substances: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'
	SET IDENTITY_INSERT dbo.MC_Substances OFF

	UPDATE S
	   SET [Substance] = I.[Substance]
		  ,[Symbol] = I.[Symbol]
		  ,[CAS] = I.[CAS]
		  ,[EC] = I.[EC]
		  ,[JGPSSI] = I.[JGPSSI]
		  ,[SubstanceType_ID] = I.[SubstanceType_ID]
		  ,[SubstanceCode_ID] = I.[SubstanceCode_ID]
		  ,[JigSubstanceCode_ID] = I.[JigSubstanceCode_ID]
		  ,[IecSubstanceType_ID] = I.[IecSubstanceType_ID]
		  ,[IecSubstanceCode_ID] = I.[IecSubstanceCode_ID]
		  ,[SVHC_Level] = I.[SVHC_Level]
		  ,[ReachSVHC] = I.[ReachSVHC]
		  ,[Reach_ID] = I.[Reach_ID]
		  ,[SVHC_Auth] = I.[SVHC_Auth]
		  ,[SVHC_SunsetDate] = I.[SVHC_SunsetDate]
		  ,[KREACH] = I.[KREACH]
		  ,[RoHS_Substance] = I.[RoHS_Substance]
		  ,[Halogen] = I.[Halogen]
		  ,[ConflictMetal] = I.[ConflictMetal]
		  ,[Proprietary] = I.[Proprietary]
		  ,[BoseProprietary] = I.[BoseProprietary]
		  ,[BoseVerified] = I.[BoseVerified]
		  ,[Comments] = I.[Comments]
		  ,[Prop65] = I.[Prop65]
		  ,[Japan_Substance] = I.[Japan_Substance]
		  ,[ABB] = I.[ABB]
		  ,[Active] = I.[Active]
		  ,[CreatedBy] = I.[CreatedBy]
		  ,[CreateDate] = I.[CreateDate]
		  ,[LWhoDid] = I.[LWhoDid]
		  ,[LastUpdate] = I.[LastUpdate]
		  ,[Toxicity] = I.[Toxicity]
		  ,[NSRL_MADL] = I.[NSRL_MADL]
	FROM dbo.MC_Substances S
	JOIN SQL3.AppCity.dbo.MC_Substances I ON S.[Substance_ID] = I.[Substance_ID]
	PRINT 'UPDATE MC_Substances: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'

	-- Copy main content record
	------------------------------------------------------------------------------
	SET IDENTITY_INSERT dbo.MC_Contents ON
	INSERT INTO dbo.MC_Contents (
		[Content_ID],
		[Manufacturer_ID],[Package_ID],[MfgPackageDesignator],[Package],[Pins],[Series_ID],[Series],[JEDEC],[IEC],[JEITA],[MaterialsSpecification_ID],[MaterialsSpecification_ID2],
		[PC_ID],[IsDefault],[IsLeadFree],[CompliantAlternateAvailable],[BaseAlloy_ID],[Plating],[Plating_ID],[SiteCode],[SiteLocation],[SiteComments],[Description],
		[meas_MassMin],[meas_MassTyp],[meas_MassMax],[calc_MassMin],[calc_MassTyp],[calc_MassMax],[UnitType_ID],[Flag],[FinishType_ID],[PostProcess_ID],[Underlayer_ID],[MSL_ID],
		[SolderMethod],[PeakReflow],[MaxTimeAtPeak],[NumReflowCycles],[JesdClass_ID],[HalogenFree],[RoHS_ID],[RoHS_NonCompliant_ID],[ChinaRoHS_EFUP_ID],[SvhcFree],
		[HasProprietarySubstance],[HasConflictMinerals],[HasGold],[HasTantalum],[HasTin],[HasTungsten],[DecaBdeFree],[PfosFree],[IsBattery],[W18],[DeclarationType_ID],
		[HasFullDisclosure],[ConflictMineral_ID],[Approved],[Active],[Comments],[CmmAccount_Id],[IsPreferredContent],[BosChangeDate],[VerifiedBy],[VerifiedTimestamp],
		[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Prop65SubstancePresent],[MaxWaveSolderTemp],[MaxTotalWaveTime],[NumWaveCycles],[HasConflictMetals],[CM_InRegion],
		[CM_PotentiallyInRegion],[HasConflictDoc],[HasEICCGeSIDoc],[CMReportVersion],[Proprietary_Derived],[HalogenFree_Derived],[RoHS],[RoHS_Derived],
		[RoHS_NonCompliant_Derived],[RoHS4Present_Derived],[SVHCFree_Derived],[SVHC_SunsetDate_Derived],[HasExemptions],[JIG_RAI],[ResponseDate],[ChinaRoHS_EFUP],[Prop65Present_Derived])
	SELECT
		[Content_ID],
		[Manufacturer_ID],[Package_ID],[MfgPackageDesignator],[Package],[Pins],[Series_ID],[Series],[JEDEC],[IEC],[JEITA],[MaterialsSpecification_ID],[MaterialsSpecification_ID2],
		[PC_ID],[IsDefault],[IsLeadFree],[CompliantAlternateAvailable],[BaseAlloy_ID],[Plating],[Plating_ID],[SiteCode],[SiteLocation],[SiteComments],[Description],
		[meas_MassMin],[meas_MassTyp],[meas_MassMax],[calc_MassMin],[calc_MassTyp],[calc_MassMax],[UnitType_ID],[Flag],[FinishType_ID],[PostProcess_ID],[Underlayer_ID],[MSL_ID],
		[SolderMethod],[PeakReflow],[MaxTimeAtPeak],[NumReflowCycles],[JesdClass_ID],[HalogenFree],[RoHS_ID],[RoHS_NonCompliant_ID],[ChinaRoHS_EFUP_ID],[SvhcFree],
		[HasProprietarySubstance],[HasConflictMinerals],[HasGold],[HasTantalum],[HasTin],[HasTungsten],[DecaBdeFree],[PfosFree],[IsBattery],[W18],[DeclarationType_ID],
		[HasFullDisclosure],[ConflictMineral_ID],[Approved],[Active],[Comments],[CmmAccount_Id],[IsPreferredContent],[BosChangeDate],[VerifiedBy],[VerifiedTimestamp],
		[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Prop65SubstancePresent],[MaxWaveSolderTemp],[MaxTotalWaveTime],[NumWaveCycles],[HasConflictMetals],[CM_InRegion],
		[CM_PotentiallyInRegion],[HasConflictDoc],[HasEICCGeSIDoc],[CMReportVersion],[Proprietary_Derived],[HalogenFree_Derived],[RoHS],[RoHS_Derived],
		[RoHS_NonCompliant_Derived],[RoHS4Present_Derived],[SVHCFree_Derived],[SVHC_SunsetDate_Derived],[HasExemptions],[JIG_RAI],[ResponseDate],[ChinaRoHS_EFUP],[Prop65Present_Derived]
	FROM SQL3.AppCity.dbo.MC_Contents
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_Contents: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'
	SET IDENTITY_INSERT dbo.MC_Contents OFF

	-- Copy content tree
	------------------------------------------------------------------------------
	DECLARE @NumComponents INT = 0
	DECLARE @NumMaterials  INT = 0
	DECLARE @NumSubstances INT = 0
	
	DECLARE @OldContentComponent_ID INT
	DECLARE level1Cursor CURSOR FOR
		SELECT [ContentComponent_ID] FROM SQL3.AppCity.dbo.MC_ContentComponents WHERE [Content_ID] = @ContentID
	OPEN level1Cursor
	FETCH NEXT FROM level1Cursor INTO @OldContentComponent_ID
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
			DECLARE @NewContentComponent_ID INT
			INSERT INTO dbo.MC_ContentComponents (
				[Content_ID],[Component_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
				[MassPPM],[MassPPMMin],[MassPPMMax],[Comments],[Entry_CC_ID],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate])
			SELECT
				[Content_ID],[Component_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
				[MassPPM],[MassPPMMin],[MassPPMMax],[Comments],[Entry_CC_ID],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate]
			FROM SQL3.AppCity.dbo.MC_ContentComponents
			WHERE [ContentComponent_ID] = @OldContentComponent_ID

			SET @NewContentComponent_ID = Scope_Identity()
			
			SET @NumComponents = @NumComponents + 1
			
			------------------------------------------------------------------------------
			DECLARE @OldComponentMaterial_ID INT
			DECLARE level2Cursor CURSOR FOR
				SELECT [ComponentMaterial_ID] FROM SQL3.AppCity.dbo.MC_ComponentMaterials WHERE [ContentComponent_ID] = @OldContentComponent_ID
			OPEN level2Cursor
			FETCH NEXT FROM level2Cursor INTO @OldComponentMaterial_ID
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
					DECLARE @NewComponentMaterial_ID INT
					INSERT INTO dbo.MC_ComponentMaterials (
						[ContentComponent_ID],[Material_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
						[MassPPM],[MassPPMMin],[MassPPMMax],[Comments],[Entry_CM_ID],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate])
					SELECT
						@NewContentComponent_ID AS [ContentComponent_ID],[Material_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
						[MassPPM],[MassPPMMin],[MassPPMMax],[Comments],[Entry_CM_ID],[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate]
					FROM SQL3.AppCity.dbo.MC_ComponentMaterials
					WHERE [ComponentMaterial_ID] = @OldComponentMaterial_ID

					SET @NewComponentMaterial_ID = Scope_Identity()
					
					SET @NumMaterials = @NumMaterials + 1

					--============================================================================
					DECLARE @OldMaterialSubstance_ID INT
					DECLARE level3Cursor CURSOR FOR
						SELECT [MaterialSubstance_ID] FROM SQL3.AppCity.dbo.MC_MaterialSubstances WHERE [ComponentMaterial_ID] = @OldComponentMaterial_ID
					OPEN level3Cursor
					FETCH NEXT FROM level3Cursor INTO @OldMaterialSubstance_ID
					WHILE (@@FETCH_STATUS = 0)
					BEGIN
							INSERT INTO dbo.MC_MaterialSubstances (
								[ComponentMaterial_ID],[Substance_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
								[MassPPM],[MassPPMMin],[MassPPMMax],[TotalMassPPM],[TotalMassPPMMin],[TotalMassPPMMax],[IPC_ID],[IPC_ID2],[IPC_Med_ID],[Comments],
								[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Exemption_ID],[Exemption_ID2],[Exemption_MED_ID])
							SELECT
								@NewComponentMaterial_ID AS [ComponentMaterial_ID],[Substance_ID],[PC_ID],[Mass],[MassMin],[MassMax],[MassPercentage],[MassPercentageMin],[MassPercentageMax],
								[MassPPM],[MassPPMMin],[MassPPMMax],[TotalMassPPM],[TotalMassPPMMin],[TotalMassPPMMax],[IPC_ID],[IPC_ID2],[IPC_Med_ID],[Comments],
								[Active],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate],[Exemption_ID],[Exemption_ID2],[Exemption_MED_ID]
							FROM SQL3.AppCity.dbo.MC_MaterialSubstances
							WHERE [MaterialSubstance_ID] = @OldMaterialSubstance_ID
							
							SET @NumSubstances = @NumSubstances + 1

							FETCH NEXT FROM level3Cursor INTO @OldMaterialSubstance_ID
					END
					CLOSE level3Cursor
					DEALLOCATE level3Cursor
					--============================================================================
					FETCH NEXT FROM level2Cursor INTO @OldComponentMaterial_ID
			END
			CLOSE level2Cursor
			DEALLOCATE level2Cursor
			------------------------------------------------------------------------------
			FETCH NEXT FROM level1Cursor INTO @OldContentComponent_ID
	END
	CLOSE level1Cursor
	DEALLOCATE level1Cursor
	
	PRINT 'INSERT ContentTree: '
	PRINT '     ' + ISNULL(CAST(@NumComponents AS VARCHAR(50)), 'NULL') + ' Components.'
	PRINT '     ' + ISNULL(CAST(@NumMaterials AS VARCHAR(50)), 'NULL') + ' Materials.'
	PRINT '     ' + ISNULL(CAST(@NumSubstances AS VARCHAR(50)), 'NULL') + ' Substances.'


	-- Copy REACH
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentReachSubstances (
		[Content_ID], [Substance_ID], [AboveThreshold], [Weight], [PPM], [Description])
	SELECT
		[Content_ID], [Substance_ID], [AboveThreshold], [Weight], [PPM], [Description]
	FROM SQL3.AppCity.dbo.MC_ContentReachSubstances
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentReachSubstances: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'
	
	
	-- Copy China RoHS
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentChinaRoHS (
		[Content_ID], [ChinaRohs_ID], [AboveThreshold])
	SELECT
		[Content_ID], [ChinaRohs_ID], [AboveThreshold]
	FROM SQL3.AppCity.dbo.MC_ContentChinaRoHS
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentChinaRoHS: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'
	
	
	-- Copy IEC-62474
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentIEC62474RefSubstances (
		[Content_ID], [ID], [AboveThreshold])
	SELECT
		[Content_ID], [ID], [AboveThreshold]
	FROM SQL3.AppCity.dbo.MC_ContentIEC62474RefSubstances
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentIEC62474RefSubstances: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'


	-- Copy Prop 65
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentProp65Substances (
		[Content_ID], [Substance_ID], [IsPresent])
	SELECT
		[Content_ID], [Substance_ID], [IsPresent]
	FROM SQL3.AppCity.dbo.MC_ContentProp65Substances
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentProp65Substances: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'
	
	
	-- Copy exemptions
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentExemptions (
		[Content_ID], [Exemption_ID], [CreatedBy], [CreateDate], [LWhoDid], [LastUpdate])
	SELECT
		[Content_ID], [Exemption_ID], [CreatedBy], [CreateDate], [LWhoDid], [LastUpdate]
	FROM SQL3.AppCity.dbo.MC_ContentExemptions
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentExemptions: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'


	-- Copy documents
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_ContentDocuments(
		[Content_ID], [Document_ID], [CreatedBy], [CreateDate])
	SELECT
		[Content_ID], [Document_ID], [CreatedBy], [CreateDate]
	FROM SQL3.AppCity.dbo.MC_ContentDocuments
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_ContentDocuments: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'


	-- Copy pn's
	------------------------------------------------------------------------------
	INSERT INTO dbo.MC_PNs (
		[Manufacturer_ID],[MfgPN],[PNWP],[Content_ID],[CompliantPN_ID],[CompliantPN],[CompliantDateCode],[HFDateCode],[IsCustom],[Historical],
		[Mass],[MassMin],[MassMax],[Active],[VerifiedBy],[VerifiedTimeStamp],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate])
	SELECT
		[Manufacturer_ID],[MfgPN],[PNWP],[Content_ID],[CompliantPN_ID],[CompliantPN],[CompliantDateCode],[HFDateCode],[IsCustom],[Historical],
		[Mass],[MassMin],[MassMax],[Active],[VerifiedBy],[VerifiedTimeStamp],[CreatedBy],[CreateDate],[LWhoDid],[LastUpdate]
	FROM SQL3.AppCity.dbo.MC_PNs
	WHERE [Content_ID] = @ContentID
	PRINT 'INSERT MC_PNs: ' + ISNULL(CAST(@@ROWCOUNT AS VARCHAR(50)), 'NULL') + ' records.'

	--############################################################
	
	IF (@TranCounter = 0)
		COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	SET IDENTITY_INSERT dbo.MC_Substances OFF
	SET IDENTITY_INSERT dbo.MC_Contents OFF
	
	IF (@TranCounter = 0)
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
		IF XACT_STATE() <> -1
			ROLLBACK TRANSACTION ProcedureSave;
		END

	-- After the appropriate rollback, echo error information to the caller.
	DECLARE @ErrorMessage  NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState    INT;
	SELECT
		  @ErrorMessage		= ERROR_MESSAGE()
		, @ErrorSeverity	= ERROR_SEVERITY()
		, @ErrorState		= ERROR_STATE()

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
