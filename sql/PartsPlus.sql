-- Actives
IF (OBJECT_ID('tempdb..#List') IS NOT NULL) DROP TABLE #List
SELECT DISTINCT
	L.LIST, L.REFPN
INTO #List
FROM ((PALPROLST L INNER JOIN PALPROBOM B on L.REFPN = B.REFPN)
INNER JOIN Parts..CatPNsAll C
		ON (B.FN = C.FN)
		AND (B.Generic = C.Generic)
		AND (B.Package = C.Package)
		AND (B.Pins = C.Pins)
		)
WHERE 1=1
AND B.FN IS NOT NULL


SELECT List, RefPN FROM #List
ORDER BY List, RefPN

-- Passives
SELECT B.List, D.RefPN
FROM dbo.BOM_BOMs B
JOIN dbo.BOM_BOMsData D ON B.[BOM_ID] = D.[BOM_ID]
WHERE 1=1
AND Customer = 'PALPRO'
AND D.PartType_ID = 2
ORDER BY List, RefPN

--Unknowns
SELECT B.List, D.RefPN
FROM dbo.BOM_BOMs B
JOIN dbo.BOM_BOMsData D ON B.[BOM_ID] = D.[BOM_ID]
WHERE 1=1
AND Customer = 'PALPRO'
AND D.PartType_ID = 0
ORDER BY List, RefPN
