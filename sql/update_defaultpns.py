import os, sys
import time, logging
import psycopg2
from utils.db_utils import get_conn
from log_controller import log_setup, fancy_time


def main():
    ''' all of the bom parts in cmm need to have their default pn field updated
        according to the rules laid out in the legacy app.abs
    '''
    log_setup()
    logging.info("**************Starting default pn update script*****")
    # get all the boms
    conn = get_conn('CMM_DB')

    bom_sql = """ SELECT b.id from BOM b 
                  INNER JOIN boms_parts_data bpd ON (b.id = bpd.bom_id)
                  GROUP BY b.id HAVING count(bpd.id) > 1
                  ORDER BY b.id ASC"""
    with conn.cursor() as cur:
        cur.execute(bom_sql)
        for r in cur.fetchall():
            bomid = r[0]
            logging.info("Working on bom {b}".format(b=bomid))
            clear_vals(bomid)
            fix_bom(bomid)
    conn.close()

    # First set everything to 0

def fix_bom(bom_id):
    ''' take care of just this bom '''
    conn = get_conn('CMM_DB')
    logging.info("running part one")
    sql = """ 
        UPDATE boms_parts_data as bpd 
        SET defaultpn = 'true' FROM (
            SELECT bpd.id as inner_bom_id, pd.id, pd.refpn, bpd.defaultpn, 
            ROW_NUMBER() OVER (
                PARTITION BY pd.refpn
                ORDER BY c.ispreferredcontent DESC, c.rohsderived DESC, c.rohsnoncompliantderrived DESC,
                        c.declarationtypeid ASC,
                CASE WHEN LOWER(c.svhcfreederived) = 'no' THEN 1
                     WHEN (LOWER(c.svhcfreederived) = 'no' OR LOWER(c.svhcfreederived) = 'a') THEN 0
                     ELSE c.svhcfree END DESC,
                c.proprietaryderived DESC, pd.contentid 
            ) as row_id   
            FROM boms_parts_data bpd
            INNER JOIN parts_data pd ON (bpd.parts_data_id = pd.id)
            INNER JOIN CONTENT c ON (c.id = pd.CONTENTID)
            WHERE bpd.bom_id = {b}
        ) as z
        WHERE (bpd.id = z.inner_bom_id AND bpd.bom_id = {b} AND z.row_id = 1)
    """.format(b=bom_id)

    with conn.cursor() as cur:
        cur.execute(sql)
    conn.commit()

    # second pass
    # this might could be combined
    logging.info("running part two")
    sql = """
        WITH remaining_parts as (
            SELECT pd.refpn FROM boms_parts_data bpdo
            INNER JOIN parts_data pd ON (pd.id = bpdo.parts_data_id)
            WHERE bpdo.bom_id = {b}
            GROUP BY pd.refpn
            HAVING SUM(CASE WHEN bpdo.defaultpn = 'true' THEN 1 ELSE 0 END) < 1
        )
        UPDATE boms_parts_data
        SET defaultpn = 'true' FROM (
            SELECT pd.refpn, ibpd.id as inner_bom_id, ROW_NUMBER() OVER (
            PARTITION BY pd.refpn
            ORDER BY pd.refpn, pd.catalogpn DESC, pd.manufacturer DESC 
        ) AS row_id
        FROM boms_parts_data ibpd
        INNER JOIN parts_data pd ON (ibpd.parts_data_id = pd.id)
        WHERE pd.refpn IN (
            SELECT r.refpn from remaining_parts as r
        )
        AND ibpd.bom_id = 19465150
        ) as z
        WHERE (
            id = z.inner_bom_id 
            AND bom_id = {b} 
            AND z.row_id = 1
        )
    """.format(b=bom_id)

    with conn.cursor() as cur:
        cur.execute(sql)
    conn.commit()
    conn.close()

def clear_vals(bom_id):
    ''' set all defaultpns to 'false' '''
    conn = get_conn('CMM_DB')

    sql = """ UPDATE boms_parts_data
              SET defaultpn = 'false'
              WHERE bom_id = {b} 
          """.format(b=bom_id)
    with conn.cursor() as cur:
        cur.execute(sql)
        conn.commit()
    conn.close()


if __name__ == "__main__":
    main()