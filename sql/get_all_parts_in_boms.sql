select BomName as 'Bom Name', pd.RefPN as 'Internal PN', pd.OriginalPN as 'Catalog PN', pd.originalMFG as Supplier
from BOMsPartList bpl
join BOMs b on bpl.BomId = b.BomId
join PartsData pd on bpl.PartId = pd.PartID
where b.AccountId = '80D71620-2D5A-4C80-BCB4-AA2FFCF28F20' and b.BomName != 'Master Research List'
order by [Bom Name];
