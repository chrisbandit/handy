/*
 * This is meant to be run on the DB-01 server.
 * Reference ticket : LEG-934
 * https://totalpartsplus.atlassian.net/browse/LEG-934
 */

USE WeeklyUpdate
GO

DECLARE @Customer VARCHAR(150) = 'PerkinElmer'

IF (OBJECT_ID('tempdb..#LegacyCmm_FromEC') IS NOT NULL) DROP TABLE #LegacyCmm_FromEC
SELECT
      [Customer]        = ISNULL(I.[Customer], '')
    , I.[Account_ID]
    , [RefPN]           = ISNULL([IPN], '')
    , [C_Manufacturer]  = ISNULL([Supplier], '')
    , [C_MfgPN]         = ISNULL([CatalogPN], '')
    , [ResearchStatus]  = ISNULL([Completed], '')
    , [Researched]      = 0
INTO #LegacyCmm_FromEC
FROM SQL3.EC.dbo.view_Records4LegecyCMM I
WHERE [Customer] = @Customer;


UPDATE #LegacyCmm_FromEC
SET [Researched] = CASE WHEN LEN([ResearchStatus]) > 0 THEN 1 ELSE 0 END



-- Update CMM with the new Research Statuses
UPDATE pd SET
      [ResearchStatus] = t.[ResearchStatus]
    , [Researched]     = t.[Researched]
FROM CMM.dbo.PartsData pd
JOIN #LegacyCmm_FromEC t
         ON pd.[AccountId]   = t.[Account_ID]
        AND pd.[RefPN]       = t.[RefPN]
        AND pd.[OriginalMFG] = t.[C_Manufacturer]
        AND pd.[OriginalPN]  = t.[C_MfgPN]
