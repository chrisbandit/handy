UPDATE ta set parts_in_new_not_in_control = (
    SELECT COUNT(*)
    FROM CMM..PartsData PDN 
    LEFT JOIN CMM_Update..PartsData PDC ON (
        PDC.AccountId = PDN.AccountId
        AND PDC.RefPN = PDN.RefPN
        AND PDC.CatalogPNWP = PDN.CatalogPNWP
        AND PDC.Manufacturer_ID = PDN.Manufacturer_ID
        AND PDC.OriginalPN = PDN.OriginalPN
        AND PDC.OriginalRevision = PDN.OriginalRevision
        AND PDC.CatalogPN = PDN.CatalogPN 
        AND PDC.Content_ID = PDN.Content_ID
    )
    WHERE PDN.AccountId = ta.AccountID
          AND PDC.AccountId IS NULL
 ),
 parts_in_control_not_in_new = (
    SELECT COUNT(*)
    FROM CMM_Update..PartsData PDC
    LEFT JOIN CMM..PartsData PDN ON (
        PDC.AccountId = PDN.AccountId
        AND PDC.RefPN = PDN.RefPN
        AND PDC.CatalogPNWP = PDN.CatalogPNWP
        AND PDC.Manufacturer_ID = PDN.Manufacturer_ID
        AND PDC.OriginalPN = PDN.OriginalPN
        AND PDC.OriginalRevision = PDN.OriginalRevision
        AND PDC.CatalogPN = PDN.CatalogPN 
        AND PDC.Content_ID = PDN.Content_ID
    )
    WHERE PDC.AccountId = ta.AccountID
          AND PDN.AccountId IS NULL
 ),
part_data_count_control = (
     SELECT COUNT(1) from CMM_Update..PartsData PD where ta.AccountID = PD.AccountId
),
part_data_count_new = (
     SELECT COUNT(1) from CMM..PartsData PD where ta.AccountID = PD.AccountId
)
FROM temp_AllAccounts_chris ta

CREATE TABLE temp_AllAccounts_chris (
    AccountID uniqueidentifier,
    AccountName varchar(MAX),
    part_data_count_control int default 0,
    part_data_count_new int default 0,
    parts_in_new_not_in_control int default 0,
    parts_in_control_not_in_new int default 0
)

DROP TABLE temp_AllAccounts_chris

INSERT INTO temp_AllAccounts_chris (AccountID, AccountName)
select distinct C.AccountID, C.Account from WeeklyUpdate..CmmBomData C

select * from temp_AllAccounts_chris tac
order by tac.extra_part_count DESC

SELECT * FROM PartsData PD
WHERE PD.AccountId = '25424A47-86C9-49BB-9E8E-FE540B621CDA'

    JOIN CMM_NS..BOMsPartList BPL ON (BPL.PartId = PDC.PartId)
    JOIN CMM_NS..BOMs B ON (BPL.BomId = B.BomId)


    SELECT *
    FROM CMM_ControlCopy_NS..PartsData PDC
    JOIN aspnet_Accounts A ON (A.AccountId = PDC.AccountId)
    LEFT JOIN CMM_NS..PartsData PDN ON (
        PDC.AccountId = PDN.AccountId
        AND PDC.RefPN = PDN.RefPN
        AND PDC.CatalogPNWP = PDN.CatalogPNWP
        AND PDC.Manufacturer_ID = PDN.Manufacturer_ID
        AND PDC.OriginalPN = PDN.OriginalPN
        AND PDC.OriginalRevision = PDN.OriginalRevision
        AND PDC.CatalogPN = PDN.CatalogPN 
        AND PDC.Content_ID = PDN.Content_ID
    )
    WHERE A.AccountName = 'Bose'
    AND PDN.AccountId IS NULL
    AND PDN.PartID IN (
        SELECT DISTINCT PartID FROM CMM_NS..BOMsPartList BPL
        JOIN BOMs B ON (BPL.BomId = B.BomId)
        JOIN aspnet_Accounts A2 ON (A2.AccountId = B.AccountId)
        WHERE A2.AccountName = 'Bose'
        AND B.IsCustomer = 1 
    )
   
    