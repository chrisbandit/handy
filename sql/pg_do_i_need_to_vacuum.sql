SELECT relname,
         n_dead_tup,
         n_tup_ins,
         n_tup_upd,
         n_tup_del,
         last_autoanalyze,
         autoanalyze_count
FROM pg_stat_user_tables
WHERE last_autoanalyze IS NOT NULL --
        AND relname = 'logs'
ORDER BY  last_autoanalyze DESC; 