SELECT t.relname AS table_name,
         i.relname AS index_name,
         a.attname AS column_name,
FROM pg_class t
JOIN pg_attribute a
    ON a.attrelid = t.oid
JOIN pg_index ix
    ON t.oid = ix.indrelid
        AND a.attnum = ANY(ix.indkey)
JOIN pg_class i
    ON i.oid = ix.indexrelid
WHERE t.relkind = 'r'
        AND t.relname IN ( '<table name>') ;