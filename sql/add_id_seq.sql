    CREATE SEQUENCE {seq_name}
    
    ALTER TABLE {tbl} ALTER COLUMN id
    SET DEFAULT nextval('seq_name')
    
    ALTER SEQUENCE {seq_name} OWNED BY {tbl}.{id}