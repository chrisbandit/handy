INSERT INTO boms_parts_data (bom_id, utverified, createddate, parts_data_id, quantity)
SELECT <NEWbomid>, false, NOW(), parts_data_id, 1
FROM boms_parts_data bpd 
INNER JOIN BOM b ON (b.id = bpd.bom_id) --whatever criteria
INNER JOIN parts_data pd ON (pd.id = bpd.parts_data_id) 
INNER JOIN content c ON (pd.contentid = c.id) 
INNER JOIN content_documents cd ON (cd.content_id = pd.contentid) 
INNER JOIN documents d ON ((d.id = cd.document_id)AND (d.accountid = b.accountid OR d.accountid = '00000000-0000-0000-0000-000000000000')) 
INNER JOIN document_type_map dtm ON ( dtm.document_id = cd.document_id AND dtm.doc_type_id = 25 ) 
WHERE bpd.bom_id = <OLDbomid> 
AND (d.revision != c.cmrreportversion) limit 15