SELECT so.name,
         sc.name
FROM sys.objects AS so
INNER JOIN sys.columns AS sc
    ON (sc.object_id = so.object_id)
WHERE so.type_desc = 'USER_TABLE'
        AND LOWER(sc.name) LIKE '%status%'
ORDER BY  so.name, sc.name ;