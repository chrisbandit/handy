USE [CVG]
GO

/****** Object:  Table [dbo].[CVG_ActionResponse]    Script Date: 09/25/2017 10:55:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActionResponse](
	[ActionResponse_ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionResponseType] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_ActionResponse] PRIMARY KEY CLUSTERED
(
	[ActionResponse_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[ActionType](
	[ActionTaken_ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionTakenType] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_Action_Response] PRIMARY KEY CLUSTERED
(
	[ActionTaken_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Archive](
	[ArchivedReason] [varchar](120) NULL,
	[ArchivedBy] [int] NULL,
	[ArchivedDate] [datetime] NULL,
	[DataCollection_ID] [int] NOT NULL,
	[Source] [varchar](50) NOT NULL,
	[SourceDesc] [varchar](100) NOT NULL,
	[Source_ID1] [int] NOT NULL,
	[Source_ID2] [int] NOT NULL,
	[PartType_ID] [int] NOT NULL,
	[C_RefPN] [varchar](50) NOT NULL,
	[C_MFG] [varchar](150) NOT NULL,
	[C_MfgPN] [varchar](150) NOT NULL,
	[C_PNWP] [varchar](150) NOT NULL,
	[C_Description] [varchar](80) NOT NULL,
	[MfgURL] [varchar](500) NOT NULL,
	[TPP_Mfg_ID] [int] NOT NULL,
	[SuggestedMFG] [varchar](50) NOT NULL,
	[TPP_MfgPN] [varchar](32) NOT NULL,
	[TPP_PNWP] [varchar](32) NOT NULL,
	[Content_ID] [int] NULL,
	[Validation_ID] [int] NOT NULL,
	[DataSheetURL] [varchar](500) NOT NULL,
	[DS_ID] [int] NULL,
	[PartStatus] [int] NOT NULL,
	[NRND] [bit] NOT NULL,
	[LOT] [datetime] NULL,
	[RefPNAvailability] [varchar](50) NULL,
	[Comment] [varchar](500) NOT NULL,
	[VerifiedVia_ID] [int] NOT NULL,
	[VerifiedTimeStamp] [datetime] NULL,
	[SDAChange] [varchar](6) NULL,
	[Compliance_ID] [int] NULL,
	[Alternate] [varchar](32) NOT NULL,
	[AlternateAvailURL] [varchar](500) NOT NULL,
	[AlternateAvailDate] [datetime] NULL,
	[ReplacementPn] [varchar](32) NULL,
	[RoadmapURL] [varchar](500) NOT NULL,
	[ContentDeclarationURL] [varchar](500) NOT NULL,
	[HalogenFreeURL] [varchar](500) NOT NULL,
	[SvhcURL] [varchar](500) NOT NULL,
	[SgsStatusURL] [varchar](500) NOT NULL,
	[MsdsStatusURL] [varchar](500) NOT NULL,
	[ConflictMineralsURL] [varchar](500) NOT NULL,
	[DataCollectionStatus_ID] [int] NOT NULL,
	[NCA_ID] [int] NULL,
	[RecontactMcCount] [int] NOT NULL,
	[Technician_ID] [int] NOT NULL,
	[CEGTechnician_ID] [int] NOT NULL,
	[CheckedOutBy] [int] NULL,
	[MCApproval] [bit] NOT NULL,
	[MCApprovedDate] [datetime] NULL,
	[Approval] [bit] NOT NULL,
	[ApprovedDate] [datetime] NULL,
	[TechnicianWorkedDate] [datetime] NULL,
	[RecordDueDate] [datetime] NULL,
	[RecordPriority] [int] NULL,
	[ResponseStatus_ID] [int] NOT NULL,
	[MfgResponseLog_ID] [int] NOT NULL,
	[CEGActionTaken_ID] [int] NOT NULL,
	[CEGActionTakenDate] [datetime] NULL,
	[MCActionTaken_ID] [int] NOT NULL,
	[MCActionTakenDate] [datetime] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[NcaApproval] [bit] NOT NULL,
	[NcaApprovedDate] [datetime] NULL,
	[ProjectLeadComment] [varchar](500) NULL,
 CONSTRAINT [PK_CVG_Archive] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[CEGActionTaken](
	[CEGActionTaken_ID] [int] IDENTITY(1,1) NOT NULL,
	[CEGActionTaken] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_EntryReviewType] PRIMARY KEY CLUSTERED
(
	[CEGActionTaken_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Classes](
	[Class_ID] [int] IDENTITY(1,1) NOT NULL,
	[Class] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_Classes] PRIMARY KEY CLUSTERED
(
	[Class_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Compliance](
	[Compliance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Compliance] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_Compliance] PRIMARY KEY CLUSTERED
(
	[Compliance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Collection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](50) NOT NULL,
	[SourceDesc] [varchar](100) NOT NULL,
	[Source_ID1] [int] NOT NULL,
	[Source_ID2] [int] NOT NULL,
	[PartType_ID] [int] NOT NULL,
	[C_RefPN] [varchar](50) NOT NULL,
	[C_MFG] [varchar](150) NOT NULL,
	[C_MfgPN] [varchar](150) NOT NULL,
	[C_PNWP] [varchar](150) NOT NULL,
	[C_Description] [varchar](80) NOT NULL,
	[MfgURL] [varchar](500) NOT NULL,
	[TPP_Mfg_ID] [int] NOT NULL,
	[SuggestedMFG] [varchar](50) NOT NULL,
	[TPP_MfgPN] [varchar](32) NOT NULL,
	[TPP_PNWP] [varchar](32) NOT NULL,
	[Validation_ID] [int] NOT NULL,
	[DataSheetURL] [varchar](500) NOT NULL,
	[DS_ID] [int] NOT NULL,
	[PartStatus] [int] NOT NULL,
	[NRND] [bit] NOT NULL,
	[LOT] [datetime] NULL,
	[RefPNAvailability] [varchar](50) NOT NULL,
	[Comment] [varchar](500) NOT NULL,
	[VerifiedVia_ID] [int] NOT NULL,
	[VerifiedTimeStamp] [datetime] NULL,
	[SDAChange] [varchar](6) NOT NULL,
	[Compliance_ID] [int] NULL,
	[Alternate] [varchar](32) NOT NULL,
	[AlternateAvailURL] [varchar](500) NOT NULL,
	[AlternateAvailDate] [datetime] NULL,
	[ReplacementPn] [varchar](32) NOT NULL,
	[RoadmapURL] [varchar](500) NOT NULL,
	[ContentDeclarationURL] [varchar](500) NOT NULL,
	[HalogenFreeURL] [varchar](500) NOT NULL,
	[SvhcURL] [varchar](500) NOT NULL,
	[SgsStatusURL] [varchar](500) NOT NULL,
	[MsdsStatusURL] [varchar](500) NOT NULL,
	[ConflictMineralsURL] [varchar](500) NOT NULL,
	[DataCollectionStatus_ID] [int] NOT NULL,
	[Technician_ID] [int] NOT NULL,
	[CEGTechnician_ID] [int] NOT NULL,
	[CheckedOutBy] [int] NULL,
	[MCApproval] [bit] NOT NULL,
	[MCApprovedDate] [datetime] NULL,
	[Approval] [bit] NOT NULL,
	[ApprovedDate] [datetime] NULL,
	[TechnicianWorkedDate] [datetime] NULL,
	[RecordDueDate] [datetime] NULL,
	[RecordPriority] [int] NOT NULL,
	[ResponseStatus_ID] [int] NOT NULL,
	[MfgResponseLog_ID] [int] NOT NULL,
	[CEGActionTaken_ID] [int] NOT NULL,
	[CEGActionTakenDate] [datetime] NULL,
	[MCActionTaken_ID] [int] NOT NULL,
	[MCActionTakenDate] [datetime] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[NCA_ID] [int] NULL,
	[RecontactMcCount] [int] NOT NULL,
	[NcaApproval] [bit] NOT NULL,
	[NcaApprovedDate] [datetime] NULL,
	[ProjectLeadComment] [varchar](500) NOT NULL,
 CONSTRAINT [PK_CVG_Collection] PRIMARY KEY CLUSTERED
(
	[DataCollection_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Statuses](
	[Status_ID] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_DataCollectionStatuses] PRIMARY KEY CLUSTERED
(
	[Status_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[DupRecords2Process](
	[SessionKey] [uniqueidentifier] NOT NULL,
	[DataCollection_ID] [int] NOT NULL,
	[TPP_Mfg_ID] [int] NOT NULL,
	[TPP_PNWP] [varchar](32) NOT NULL,
	[AddedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_DupRecords2Process] PRIMARY KEY CLUSTERED
(
	[SessionKey] ASC,
	[DataCollection_ID] ASC,
	[TPP_Mfg_ID] ASC,
	[TPP_PNWP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Flags](
	[ID] [int] NOT NULL,
	[C_ActiveFlag] [smallint] NOT NULL,
	[C_PassiveFlag] [smallint] NOT NULL,
	[C_PcnFlag] [smallint] NOT NULL,
	[C_McFlag] [smallint] NOT NULL,
	[C_NqlFlag] [smallint] NOT NULL,
	[TPP_ActiveFlag] [smallint] NOT NULL,
	[TPP_PassiveFlag] [smallint] NOT NULL,
	[TPP_PcnFlag] [smallint] NOT NULL,
	[TPP_McFlag] [smallint] NOT NULL,
	[TPP_NqlFlag] [smallint] NOT NULL,
	[TPP_StatusDiffFlag] [smallint] NOT NULL,
	[TPP_DatasheetFlag] [smallint] NOT NULL,
	[TPP_DatasheetDiffFlag] [smallint] NOT NULL,
	[InMcOverflow] [bit] NOT NULL,
 CONSTRAINT [PK_CVG_Flags] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[MCActionTaken](
	[MCActionTaken_ID] [int] IDENTITY(1,1) NOT NULL,
	[MCActionTaken] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_MCActionTaken] PRIMARY KEY CLUSTERED
(
	[MCActionTaken_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[MCActionTaken](
	[MCActionTaken_ID] [int] IDENTITY(1,1) NOT NULL,
	[MCActionTaken] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_MCActionTaken] PRIMARY KEY CLUSTERED
(
	[MCActionTaken_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[NoteMap](
	[DataCollection_ID] [int] NOT NULL,
	[Note_ID] [int] NOT NULL,
 CONSTRAINT [PK_CVG_NoteMap] PRIMARY KEY CLUSTERED
(
	[ID] ASC,
	[Note_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[RecordPriorities](
	[PriorityValue] [int] NOT NULL,
	[PriorityText] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CVG_RecordPriorities] PRIMARY KEY CLUSTERED
(
	[PriorityValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ResponseStatuses](
	[ResponseStatus_ID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseStatus] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_ResponseStatuses] PRIMARY KEY CLUSTERED
(
	[ResponseStatus_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[TransitionsLog](
	[ID] [int] NOT NULL,
	[LastStatus_ID] [int] NOT NULL,
	[NewStatus_ID] [int] NOT NULL,
	[Current_User_ID] [int] NOT NULL,
	[Tech_ID] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ValidationComment](
	[ValidationComment_ID] [int] IDENTITY(1,1) NOT NULL,
	[ValidationComment] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LWhoDid] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_CVG_ValidationComment] PRIMARY KEY CLUSTERED
(
	[ValidationComment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- ALTER TABLE [dbo].[DC_ValidationComment] ADD  CONSTRAINT [DF_DC_ValidationComment_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_ValidationComment] ADD  CONSTRAINT [DF_DC_ValidationComment_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_ValidationComment] ADD  CONSTRAINT [DF_DC_ValidationComment_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_TransitionsLog] ADD  CONSTRAINT [DF_DC_TransitionsLog_LastDCStatus_ID]  DEFAULT ((-1)) FOR [LastDCStatus_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_TransitionsLog] ADD  CONSTRAINT [DF_DC_TransitionsLog_NewDCStatus_ID]  DEFAULT ((-1)) FOR [NewDCStatus_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_TransitionsLog] ADD  CONSTRAINT [DF_DC_TransitionsLog_Current_User_ID]  DEFAULT ((-1)) FOR [Current_User_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_TransitionsLog] ADD  CONSTRAINT [DF_DC_TransitionsLog_User_ID]  DEFAULT ((-1)) FOR [Tech_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_TransitionsLog] ADD  CONSTRAINT [DF_DC_TransitionsLog_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_ResponseStatuses] ADD  CONSTRAINT [DF_DC_ResponseStatuses_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_ResponseStatuses] ADD  CONSTRAINT [DF_DC_ResponseStatuses_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_ResponseStatuses] ADD  CONSTRAINT [DF_DC_ResponseStatuses_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_Active]  DEFAULT ((1)) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_Active]  DEFAULT ((1)) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_MCActionTaken] ADD  CONSTRAINT [DF_DC_MCActionTaken_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_C_ActiveFlag]  DEFAULT ((0)) FOR [C_ActiveFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_C_PassiveFlag]  DEFAULT ((0)) FOR [C_PassiveFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_C_PcnFlag]  DEFAULT ((0)) FOR [C_PcnFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_C_McFlag]  DEFAULT ((0)) FOR [C_McFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_C_NqlFlag]  DEFAULT ((0)) FOR [C_NqlFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_ActiveFlag]  DEFAULT ((0)) FOR [TPP_ActiveFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_PassiveFlag]  DEFAULT ((0)) FOR [TPP_PassiveFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_PcnFlag]  DEFAULT ((0)) FOR [TPP_PcnFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_McFlag]  DEFAULT ((0)) FOR [TPP_McFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_NqlFlag]  DEFAULT ((0)) FOR [TPP_NqlFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_ActStatusDiffFlag]  DEFAULT ((0)) FOR [TPP_StatusDiffFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_DatasheetFlag]  DEFAULT ((0)) FOR [TPP_DatasheetFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_TPP_DatasheetDiffFlag]  DEFAULT ((0)) FOR [TPP_DatasheetDiffFlag]
-- GO
--
-- ALTER TABLE [dbo].[DC_Flags] ADD  CONSTRAINT [DF_DC_Flags_InMcOverflow]  DEFAULT ((0)) FOR [InMcOverflow]
-- GO
--
-- ALTER TABLE [dbo].[DC_DupRecords2Process] ADD  DEFAULT (getdate()) FOR [AddedOn]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_DataCollectionStatuses] ADD  CONSTRAINT [DF_DC_DataCollectionStatuses_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollectionStatuses] ADD  CONSTRAINT [DF_DC_DataCollectionStatuses_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollectionStatuses] ADD  CONSTRAINT [DF_DC_DataCollectionStatuses_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_SourceDesc]  DEFAULT ('') FOR [SourceDesc]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_Source_ID1]  DEFAULT ((-1)) FOR [Source_ID1]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_Source_ID2]  DEFAULT ((-1)) FOR [Source_ID2]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_Class]  DEFAULT ((-1)) FOR [PartType_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_C_RefPN]  DEFAULT ('') FOR [C_RefPN]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_C_MFG]  DEFAULT ('') FOR [C_MFG]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_C_MfgPN]  DEFAULT ('') FOR [C_MfgPN]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_C_PNWP]  DEFAULT ('') FOR [C_PNWP]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_C_Description]  DEFAULT ('') FOR [C_Description]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_MfgURL]  DEFAULT ('NA') FOR [MfgURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_TPP_Mfg_ID]  DEFAULT ((0)) FOR [TPP_Mfg_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_TPP_Mfg]  DEFAULT ('') FOR [SuggestedMFG]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_TPP_MfgPN]  DEFAULT ('') FOR [TPP_MfgPN]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_TPP_PNWP]  DEFAULT ('') FOR [TPP_PNWP]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_Validation_ID]  DEFAULT ((-1)) FOR [Validation_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_DataSheetLink]  DEFAULT ('') FOR [DataSheetURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_DS_ID]  DEFAULT ((0)) FOR [DS_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_Status]  DEFAULT ((7)) FOR [PartStatus]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_NRND]  DEFAULT ((0)) FOR [NRND]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_RefPNAvailability]  DEFAULT ('') FOR [RefPNAvailability]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_DC_Comment]  DEFAULT ('') FOR [DC_Comment]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_VerifiedVia_ID]  DEFAULT ((-1)) FOR [VerifiedVia_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_SDAChange]  DEFAULT ('') FOR [SDAChange]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_Compliance]  DEFAULT ((-1)) FOR [Compliance_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_Alternate]  DEFAULT ('') FOR [Alternate]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_AlternataAvailURL]  DEFAULT ('') FOR [AlternateAvailURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF__DC_DataCo__Repla__217A38B8]  DEFAULT ('') FOR [ReplacementPn]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_RoadmatLink]  DEFAULT ('') FOR [RoadmapURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_ContentDeclarationLink]  DEFAULT ('') FOR [ContentDeclarationURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_HalogenFreeURL]  DEFAULT ('') FOR [HalogenFreeURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_SvhcURL]  DEFAULT ('') FOR [SvhcURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_SgsStatusURL]  DEFAULT ('') FOR [SgsStatusURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_MsdsStatusURL]  DEFAULT ('') FOR [MsdsStatusURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_ConfictMineralsURL]  DEFAULT ('') FOR [ConflictMineralsURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_DataCollectionStatus_ID]  DEFAULT ((1)) FOR [DataCollectionStatus_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_Technician_ID]  DEFAULT ((-1)) FOR [Technician_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_CEGTechnician_ID]  DEFAULT ((-1)) FOR [CEGTechnician_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_MCApproval]  DEFAULT ((0)) FOR [MCApproval]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_DCApproval]  DEFAULT ((0)) FOR [DCApproval]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_RecordPriority]  DEFAULT ((0)) FOR [RecordPriority]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_ResponseStatus_ID]  DEFAULT ((-1)) FOR [ResponseStatus_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_MfgResponseLog_ID]  DEFAULT ((-1)) FOR [MfgResponseLog_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_CEGActionTaken_ID]  DEFAULT ((-1)) FOR [CEGActionTaken_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_MCActionTaken_ID]  DEFAULT ((-1)) FOR [MCActionTaken_ID]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DS_DataCollection_CreatedDate_1]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  CONSTRAINT [DF_DC_DataCollection_RecontactMcCount]  DEFAULT ((0)) FOR [RecontactMcCount]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  DEFAULT ((0)) FOR [NcaApproval]
-- GO
--
-- ALTER TABLE [dbo].[DC_DataCollection] ADD  DEFAULT ('') FOR [ProjectLeadComment]
-- GO
--
-- ALTER TABLE [dbo].[DC_Compliance] ADD  CONSTRAINT [DF_DC_Compliance_Active]  DEFAULT ((1)) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_Compliance] ADD  CONSTRAINT [DF_DC_Compliance_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_Compliance] ADD  CONSTRAINT [DF_DC_Compliance_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_Classes] ADD  CONSTRAINT [DF_DC_Classes_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_Classes] ADD  CONSTRAINT [DF_DC_Classes_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_Classes] ADD  CONSTRAINT [DF_DC_Classes_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_CEGActionTaken] ADD  CONSTRAINT [DF_DC_EntryReviewType_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_CEGActionTaken] ADD  CONSTRAINT [DF_DC_EntryReviewType_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_Archive_Date]  DEFAULT (getdate()) FOR [ArchivedDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_HalogenFreeURL]  DEFAULT ('') FOR [HalogenFreeURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_]  DEFAULT ('') FOR [SvhcURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_SgsStatusURL]  DEFAULT ('') FOR [SgsStatusURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_MsdsStatusURL]  DEFAULT ('') FOR [MsdsStatusURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_ConfictMineralsURL]  DEFAULT ('') FOR [ConflictMineralsURL]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF__DC_Archiv__Recon__79C14D19]  DEFAULT ((0)) FOR [RecontactMcCount]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_MCApproval]  DEFAULT ((0)) FOR [MCApproval]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  CONSTRAINT [DF_DC_Archive_DCApproval]  DEFAULT ((0)) FOR [DCApproval]
-- GO
--
-- ALTER TABLE [dbo].[DC_Archive] ADD  DEFAULT ((0)) FOR [NcaApproval]
-- GO
--
--
-- ALTER TABLE [dbo].[DC_ActionType] ADD  CONSTRAINT [DF_DC_ActionResponse_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[DC_ActionType] ADD  CONSTRAINT [DF_DC_ActionResponse_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[DC_ActionType] ADD  CONSTRAINT [DF_DC_ActionResponse_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
--
-- SET ANSI_PADDING OFF
-- GO
--
-- ALTER TABLE [dbo].[CVG_ActionResponse] ADD  CONSTRAINT [DF_CVG_FollowUpResponse_Active]  DEFAULT (1) FOR [Active]
-- GO
--
-- ALTER TABLE [dbo].[CVG_ActionResponse] ADD  CONSTRAINT [DF_CVG_FollowUpResponse_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
-- GO
--
-- ALTER TABLE [dbo].[CVG_ActionResponse] ADD  CONSTRAINT [DF_CVG_FollowUpResponse_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
-- GO
