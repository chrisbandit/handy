USE CMM
GO

-- Config
--------------------------------------------------------------------------------
DECLARE @AccountID UNIQUEIDENTIFIER = '03202F61-D418-4AC9-8258-A74B680E84F4'
DECLARE @CmmBackupFilename NVARCHAR(MAX) = N'O:\SQL Backups\TOTAL-DB01\CMM\FULL\TOTAL-DB01_CMM_FULL_20170911_011054.bak' 


-- Database Restore to CMM_Update
--------------------------------------------------------------------------------
--RESTORE DATABASE [CMM_Update] 
--	FROM DISK = @CmmBackupFilename
--	WITH FILE = 1,  
--		 MOVE N'CMM2'     TO N'O:\MSSQL\Data\CMM_Update.mdf',  
--		 MOVE N'CMM2_log' TO N'O:\MSSQL\Data\CMM_Update_1.ldf',
--		 NOUNLOAD, STATS = 10, REPLACE


-- Restore Account MasterList & BOMs to live CMM
--------------------------------------------------------------------------------
--SET NOCOUNT ON;

DECLARE @ErrorMessage  NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState    INT;

BEGIN TRANSACTION;

BEGIN TRY

	-- Reload all BOM/Master List entries
	DELETE L FROM dbo.BOMsPartList L
	WHERE EXISTS (
			SELECT * FROM dbo.BOMs B WHERE B.AccountId = @AccountID
			AND B.BomId = L.BomId
			)
	DELETE FROM dbo.PartsData WHERE AccountId = @AccountID
	DELETE FROM dbo.BOMs WHERE AccountId = @AccountID

	SET IDENTITY_INSERT dbo.BOMs ON
		INSERT INTO dbo.BOMs (
			[AccountId],[BomId],[OldBomId],[BomName],[BomLocked],[CreateDate],[IsMaster],[IsCustomer],[IsExempt_Id],[IsCustomList])
		SELECT
			[AccountId],[BomId],[OldBomId],[BomName],[BomLocked],[CreateDate],[IsMaster],[IsCustomer],[IsExempt_Id],[IsCustomList]
		FROM CMM_Update.dbo.BOMs
		WHERE 1=1
		AND [AccountId] = @AccountID
	SET IDENTITY_INSERT dbo.BOMs OFF

	
	-- Load BOM permissions
	INSERT INTO dbo.BomUserMap (
		[BomId], [OldBomId], [UserId], [PermissionId])
	SELECT
		[BomId], [OldBomId], [UserId], [PermissionId]
	FROM CMM_Update.dbo.BomUserMap BUM
	WHERE EXISTS (
			SELECT * FROM dbo.BOMs B
			WHERE 1=1
			AND B.[AccountId] = @AccountID
			AND BUM.[BomId] = B.[BomId]
			)
	EXCEPT
	SELECT
		[BomId], [OldBomId], [UserId], [PermissionId]
	FROM dbo.BomUserMap BUM
	WHERE EXISTS (
			SELECT * FROM dbo.BOMs B
			WHERE 1=1
			AND B.[AccountId] = @AccountID
			AND BUM.[BomId] = B.[BomId]
			);


	-- Load Master List
	SET IDENTITY_INSERT dbo.PartsData ON
		INSERT INTO dbo.PartsData (
			[PartID],[AccountId],[BOMId],[RefPN],[CatalogPN],[CatalogPNWP],[Manufacturer],[Manufacturer_ID],[OriginalPN],[OriginalMFG],[OriginalMFG_ID],
			[OriginalDescription],[OriginalRevision],[PartStatus],[Active],[Content_ID],[HasConflictMetals],[HasConflictDoc],[Proprietary],[HalogenFree],
			[RoHS],[RoHS_NonCompliant],[RoHS4Present],[SVHCFree],[SVHC_SunsetDate],[UnitType],[Disclosure],[HasExemptions],[HasEICCGeSIDoc],[LeadFree],
			[IsMaster],[Researched],[ResearchStatus],[CreatedDate],[ExemptionDate],[CMReportVersion],[NCA_ID])
		SELECT
			[PartID],[AccountId],[BOMId],[RefPN],[CatalogPN],[CatalogPNWP],[Manufacturer],[Manufacturer_ID],[OriginalPN],[OriginalMFG],[OriginalMFG_ID],
			[OriginalDescription],[OriginalRevision],[PartStatus],[Active],[Content_ID],[HasConflictMetals],[HasConflictDoc],[Proprietary],[HalogenFree],
			[RoHS],[RoHS_NonCompliant],[RoHS4Present],[SVHCFree],[SVHC_SunsetDate],[UnitType],[Disclosure],[HasExemptions],[HasEICCGeSIDoc],[LeadFree],
			[IsMaster],[Researched],[ResearchStatus],[CreatedDate],[ExemptionDate],[CMReportVersion],[NCA_ID]
		FROM CMM_Update.dbo.PartsData
		WHERE [AccountId] = @AccountID
	SET IDENTITY_INSERT dbo.PartsData OFF

	
	-- Load BOM parts
	INSERT INTO dbo.BOMsPartList (
		[BomId],[PartId],[DefaultPn],[Quantity],[CUnitType],[UTVerified],[CreatedDate])
	SELECT 
		[BomId],[PartId],[DefaultPn],[Quantity],[CUnitType],[UTVerified],[CreatedDate]
	FROM CMM_Update.dbo.BOMsPartList L
	WHERE EXISTS (
			SELECT * FROM dbo.BOMs B
			WHERE B.[AccountId] = @AccountID
			AND B.[IsMaster] = 0
			AND B.[BomId] = L.[BomId])

	
	-- Load CM data
	INSERT INTO dbo.BOMs_CM2Update ([BomId])
	SELECT B.[BomId] 
	FROM dbo.BOMs B
	WHERE 1=1
	AND B.[AccountId] = @AccountID
	AND B.[IsMaster] = 0


	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION;

	SELECT @ErrorMessage = ERROR_MESSAGE()
	     , @ErrorSeverity = ERROR_SEVERITY()
	     , @ErrorState = ERROR_STATE();
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
