SELECT A.AccountName,
       PDN.RefPN,
       PDN.Manufacturer,
       PDN.OriginalMFG,
       PDN.OriginalPN,
       PDN.OriginalRevision,
       PDN.CatalogPN as newupdate_cpn,
       PDN.CatalogPNWP as newupdate_pnwp,
       PDO2.CatalogPN as control_cpn,
       PDO2.CatalogPNWP as control_pnwp
FROM CMM..PartsData PDN
JOIN aspnet_Accounts A ON (A.AccountId = PDN.AccountId)
LEFT JOIN CMM_ControlCopy..PartsData PDO
ON (
    PDN.RefPN = PDO.RefPN
    AND PDN.AccountId = PDO.AccountId
    AND PDN.Manufacturer = PDO.Manufacturer --TPP Supplier
	AND PDN.OriginalMFG = PDO.OriginalMFG --Supplier
    AND PDN.OriginalPN = PDO.OriginalPN --CatalogPN
	AND PDN.CatalogPN = PDO.CatalogPN --TPP CatalogPN
	AND PDN.OriginalRevision = PDO.OriginalRevision	
)
LEFT JOIN CMM_ControlCopy..PartsData PDO2 ON (
    PDN.RefPN = PDO2.RefPN
    AND PDN.AccountId = PDO2.AccountId
    AND PDN.Manufacturer = PDO2.Manufacturer --TPP Supplier
	AND PDN.OriginalMFG = PDO2.OriginalMFG --Supplier
    AND PDN.OriginalPN = PDO2.OriginalPN --CatalogPN
	AND PDN.CatalogPNWP = PDO2.CatalogPNWP --TPP CatalogPN
	AND PDN.OriginalRevision = PDO2.OriginalRevision	
)
WHERE PDO.AccountId IS NULL
ORDER BY AccountName, RefPN
