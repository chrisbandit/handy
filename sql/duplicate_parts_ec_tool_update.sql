select i.IPN, ec.CatalogPN, ec.Supplier, COUNT(*)
from dbo.EcPartData ec join
Work.dbo.[ImportTable_EC_389] i on i.Part_ID = ec.Part_ID
group by i.IPN, ec.CatalogPN, ec.Supplier
HAVING COUNT(*) > 1
