/***************************************************************
This script was written for the ticket LEG-721
https://totalpartsplus.atlassian.net/browse/LEG-721
***************************************************************/
USE AppCity;
GO

EXEC dbo.DC_UpdateSDAChange;

/***************************************************************
The code below is has been made into the stored procedure "AppCity.dbo.DC_UpdateSDAChange".
***************************************************************/
/*
-- Create temp table to calculate the new SDAChange on
IF (OBJECT_ID('tempdb..#UpdatedSDAChange') IS NOT NULL) DROP TABLE #UpdatedSDAChange
SELECT
      D.[DataCollection_ID]
    , D.[SDAChange]
    , [New_SDAChange] = CAST('' AS VARCHAR(6))
    , [Manufacturer_ID] = D.[TPP_Mfg_ID]
    , [Manufacturer] = ISNULL(M.[Manufacturer], '')
    , [MfgPN] = D.[TPP_MfgPN]
    , [PNWP] = D.[TPP_PNWP]
    , [SDASource] = CAST('' AS VARCHAR(50))
INTO #UpdatedSDAChange
FROM dbo.DC_DataCollection D
LEFT JOIN dbo.MFG_Manufacturers M ON D.[TPP_Mfg_ID] = M.[Manufacturer_ID]


-- Updates the SDAChange from parts in the Actives DB (Manufacturer, PNWP)
UPDATE T
SET   [New_SDAChange] = ISNULL(A.[SDAChange], '')
    , [SDASource] = 'Actives'
FROM #UpdatedSDAChange T
JOIN Datafix.dbo.CatPNsAll A
    ON T.[Manufacturer] = A.[Manufacturer]
    AND T.[PNWP] = A.[PNWP]
WHERE T.[New_SDAChange] = ''


-- Updates the SDAChange from parts in the Passives DB (Manufacturer, MfgPN)
UPDATE T
SET   [New_SDAChange] = ISNULL(P.[SDAChange], '')
    , [SDASource] = 'Passives'
FROM #UpdatedSDAChange T
JOIN dbo.PASV_Passives P
    ON T.[Manufacturer_ID] = P.[Manufacturer_ID]
    AND T.[MfgPN] = P.[MfgPN]
WHERE T.[New_SDAChange] = ''


-- Updates the SDAChange from parts in the Passives DB (Manufacturer, PNWP)
UPDATE T
SET   [New_SDAChange] = ISNULL(P.[SDAChange], '')
    , [SDASource] = 'Passives PNWP'
FROM #UpdatedSDAChange T
JOIN dbo.PASV_Passives P
    ON T.[Manufacturer_ID] = P.[Manufacturer_ID]
    AND T.[PNWP] = P.[PNWP]
WHERE T.[New_SDAChange] = ''


-- Remove any records that do not have a change in the SDAChange field
DELETE FROM #UpdatedSDAChange
WHERE [SDAChange] = [New_SDAChange]


-- Update the DC records that have a change in the SDAChange field
UPDATE D
SET [SDAChange] = T.[New_SDAChange]
FROM dbo.DC_DataCollection D
JOIN #UpdatedSDAChange T ON D.[DataCollection_ID] = T.[DataCollection_ID]
*/