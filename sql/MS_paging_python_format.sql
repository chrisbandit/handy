SELECT *
FROM ( 
    (SELECT ROW_NUMBER()
        OVER (ORDER BY {o} ASC) AS num, {fcols}
    FROM {jt}) ) AS pagetbl
WHERE num
    BETWEEN {s}
        AND {e}
ORDER BY  num """.format(fcols=', '.join(self.get_cols('from')), 
                 jt=job['from_table'], o=order_col, s=start, e=end) 