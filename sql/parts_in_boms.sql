--Make sure all of the parts in BOMs for a company is in the Master List--
select ml.accountid, b.accountid, count(pd.id)
	from boms_parts_data bpd
    left join parts_data pd on pd.id = bpd.parts_data_id
    left join bom b on b.id = bpd.bom_id
    left join master_list ml on pd.bomid = ml.id and b.accountid = ml.accountid
group by ml.accountid, b.accountid
order by ml.accountid, b.accountid;

select b.accountid, b.bomname, count(pd.id)
	from boms_parts_data bpd
    left join parts_data pd on pd.id = bpd.parts_data_id
    left join bom b on b.id = bpd.bom_id
    left join master_list ml on pd.bomid = ml.id and b.accountid = ml.accountid
	group by b.bomname, b.accountid
    order by b.accountid, b.bomname;

select ml.accountid, b.accountid, count(pd.id)
	from boms_parts_data bpd
    left join parts_data pd on pd.id = bpd.parts_data_id
    left join bom b on b.id = bpd.bom_id
    left join master_list ml on pd.bomid = ml.id and b.accountid = ml.accountid
	group by ml.accountid, b.accountid
    order by ml.accountid, b.accountid;

select ml.accountid, count(pd.id)
	from parts_data pd
    left join master_list ml on pd.bomid = ml.id
	group by ml.accountid
    order by ml.accountid;

select b.accountid, b.bomname, count(pd.id)
	from boms_parts_data bpd
	join bom b on b.id = bpd.bom_id
	join parts_data pd on pd.id = bpd.parts_data_id
	group by b.bomname, b.accountid
    order by b.accountid, b.bomname;
