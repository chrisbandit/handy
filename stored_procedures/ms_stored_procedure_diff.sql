
-- =============================================
-- Author:		Chris Wood
-- Create date: 8/30/2016
-- Description:	Show diff of 2 stored procedures
-- =============================================
BEGIN
    SET NOCOUNT ON;

    --The id column gives a reference for where a difference occurs
    CREATE TABLE temp_sp1_chris (
        ID INT NOT NULL IDENTITY(1,1),
        CodeTxt varchar(max)
    )
    CREATE TABLE temp_sp2_chris (
        ID INT NOT NULL IDENTITY(1,1),
        CodeTxt varchar(max)
    )
    
    -- First place the content of each sp into a table
    INSERT INTO temp_sp1_chris (CodeTxt)
        EXEC sp_HelpText @proc1

    INSERT INTO temp_sp2_chris (CodeTxt)
        EXEC sp_HelpText @proc2

    -- I have to create another temp table because you cannot "nest" 
    -- stored procedure calls. I could create a table variable but that is 
    -- going to be slower
    SELECT A.ID as 'proc1_col', 
           A.CodeTxt as 'proc1_txt', 
           B.ID as 'proc2_col', 
           B.CodeTxt as 'proc2_txt'
    INTO temp_chris_xmlapi
    FROM temp_sp1_chris A
        FULL OUTER JOIN temp_sp2_chris B 
            ON (A.CodeTxt = B.CodeTxt)
    WHERE A.CodeTxt IS NULL 
        OR B.CodeTxt IS NULL
    
    -- Now do a self join to get the differences into the same row
    SELECT A.proc1_col, A.proc1_txt, B.proc2_txt 
    FROM temp_chris_xmlapi A
        INNER JOIN temp_chris_xmlapi B
            ON (A.proc1_col = B.proc2_col)
    ORDER BY A.proc1_col

    DROP TABLE temp_sp1_chris
    DROP TABLE temp_sp2_chris
    DROP TABLE temp_chris_xmlapi
END