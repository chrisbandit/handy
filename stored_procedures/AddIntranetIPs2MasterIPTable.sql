USE [CMM]
GO
/****** Object:  StoredProcedure [dbo].[AddIntranetIPs2MasterIPTable]    Script Date: 04/05/2017 08:32:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nathen Silver
-- Create date: 12-14-2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[AddIntranetIPs2MasterIPTable] 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Ip		INT = 0
	DECLARE @IpMax	INT = 255
	DECLARE @BaseIP	VARCHAR(25) = '192.168.5.'
	
	DECLARE @BuiltIP VARCHAR(25)

	--DELETE FROM dbo.aspnet_MasterIP WHERE [AllowedIP] LIKE @BaseIP + '%'

	PRINT 'Adding missing Intranet IP addresses to aspnet_MasterIP table.'
	WHILE (@Ip <= @IpMax)
	BEGIN
			SET @BuiltIP = @BaseIP + CAST(@Ip AS VARCHAR)
			
			IF (0 = (SELECT COUNT(*) FROM dbo.aspnet_MasterIP WHERE [AllowedIP] = @BuiltIP))
			BEGIN
					INSERT INTO dbo.aspnet_MasterIP ([MasterId], [AllowedIP])
					VALUES (1, @BuiltIP)
					PRINT '	Added ' + @BuiltIP
			END

			SET @Ip = @Ip + 1
	END
	PRINT 'Completed.'

END

