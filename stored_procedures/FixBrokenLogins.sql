SET NOCOUNT ON;

IF (OBJECT_ID('tempdb..#brokenUsers') IS NOT NULL) DROP TABLE #brokenUsers;
CREATE TABLE #brokenUsers (
	UserName SYSNAME       NOT NULL,
	UserSID  VARBINARY(85) NULL);


INSERT INTO #brokenUsers
	EXEC dbo.sp_change_users_login 'report';


DECLARE @UserName NVARCHAR(100);
DECLARE UserCursor CURSOR FOR
	SELECT [UserName] FROM #brokenUsers;

PRINT 'Correcting DB Logins';
OPEN UserCursor;
FETCH NEXT FROM UserCursor INTO @UserName;
WHILE (@@FETCH_STATUS = 0)
	BEGIN
		PRINT 'LOGIN :  ' + @UserName
		EXECUTE ('EXEC dbo.sp_change_users_login ''auto_fix'', ''' + @UserName + ''' ');
		FETCH NEXT FROM UserCursor INTO @UserName;
	END
CLOSE UserCursor;

DEALLOCATE UserCursor;


SET NOCOUNT OFF;